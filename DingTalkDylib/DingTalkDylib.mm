#line 1 "/Users/sephilex/Desktop/xiaopao/DingTalk/DingTalkDylib/DingTalkDylib.xm"
#import "DingTalkHelper.h"
#import "LLPunchConfig.h"
#import "LLPunchManager.h"


#include <substrate.h>
#if defined(__clang__)
#if __has_feature(objc_arc)
#define _LOGOS_SELF_TYPE_NORMAL __unsafe_unretained
#define _LOGOS_SELF_TYPE_INIT __attribute__((ns_consumed))
#define _LOGOS_SELF_CONST const
#define _LOGOS_RETURN_RETAINED __attribute__((ns_returns_retained))
#else
#define _LOGOS_SELF_TYPE_NORMAL
#define _LOGOS_SELF_TYPE_INIT
#define _LOGOS_SELF_CONST
#define _LOGOS_RETURN_RETAINED
#endif
#else
#define _LOGOS_SELF_TYPE_NORMAL
#define _LOGOS_SELF_TYPE_INIT
#define _LOGOS_SELF_CONST
#define _LOGOS_RETURN_RETAINED
#endif

@class DTSectionItem; @class DTCellItem; @class LLPunchSettingController; @class DTTableViewController; @class LAPluginInstanceCollector; @class DTSettingListViewController; @class DTInfoPlist; @class DTWebViewController; @class LLSettingController; 
static BOOL (*_logos_orig$_ungrouped$DTWebViewController$pluginInstance$jsapiShouldCall$)(_LOGOS_SELF_TYPE_NORMAL DTWebViewController* _LOGOS_SELF_CONST, SEL, id, id); static BOOL _logos_method$_ungrouped$DTWebViewController$pluginInstance$jsapiShouldCall$(_LOGOS_SELF_TYPE_NORMAL DTWebViewController* _LOGOS_SELF_CONST, SEL, id, id); static void (*_logos_orig$_ungrouped$LAPluginInstanceCollector$handleJavaScriptRequest$callback$)(_LOGOS_SELF_TYPE_NORMAL LAPluginInstanceCollector* _LOGOS_SELF_CONST, SEL, id, void(^)(id dic)); static void _logos_method$_ungrouped$LAPluginInstanceCollector$handleJavaScriptRequest$callback$(_LOGOS_SELF_TYPE_NORMAL LAPluginInstanceCollector* _LOGOS_SELF_CONST, SEL, id, void(^)(id dic)); static void (*_logos_orig$_ungrouped$DTSettingListViewController$tidyDatasource)(_LOGOS_SELF_TYPE_NORMAL DTSettingListViewController* _LOGOS_SELF_CONST, SEL); static void _logos_method$_ungrouped$DTSettingListViewController$tidyDatasource(_LOGOS_SELF_TYPE_NORMAL DTSettingListViewController* _LOGOS_SELF_CONST, SEL); static void (*_logos_orig$_ungrouped$LLSettingController$viewDidLoad)(_LOGOS_SELF_TYPE_NORMAL LLSettingController* _LOGOS_SELF_CONST, SEL); static void _logos_method$_ungrouped$LLSettingController$viewDidLoad(_LOGOS_SELF_TYPE_NORMAL LLSettingController* _LOGOS_SELF_CONST, SEL); static void _logos_method$_ungrouped$LLSettingController$setNavigationBar(_LOGOS_SELF_TYPE_NORMAL LLSettingController* _LOGOS_SELF_CONST, SEL); static void _logos_method$_ungrouped$LLSettingController$tidyDataSource(_LOGOS_SELF_TYPE_NORMAL LLSettingController* _LOGOS_SELF_CONST, SEL); static NSString * (*_logos_meta_orig$_ungrouped$DTInfoPlist$getAppBundleId)(_LOGOS_SELF_TYPE_NORMAL Class _LOGOS_SELF_CONST, SEL); static NSString * _logos_meta_method$_ungrouped$DTInfoPlist$getAppBundleId(_LOGOS_SELF_TYPE_NORMAL Class _LOGOS_SELF_CONST, SEL); 
static __inline__ __attribute__((always_inline)) __attribute__((unused)) Class _logos_static_class_lookup$LLSettingController(void) { static Class _klass; if(!_klass) { _klass = objc_getClass("LLSettingController"); } return _klass; }static __inline__ __attribute__((always_inline)) __attribute__((unused)) Class _logos_static_class_lookup$DTWebViewController(void) { static Class _klass; if(!_klass) { _klass = objc_getClass("DTWebViewController"); } return _klass; }static __inline__ __attribute__((always_inline)) __attribute__((unused)) Class _logos_static_class_lookup$LLPunchSettingController(void) { static Class _klass; if(!_klass) { _klass = objc_getClass("LLPunchSettingController"); } return _klass; }static __inline__ __attribute__((always_inline)) __attribute__((unused)) Class _logos_static_class_lookup$DTSectionItem(void) { static Class _klass; if(!_klass) { _klass = objc_getClass("DTSectionItem"); } return _klass; }static __inline__ __attribute__((always_inline)) __attribute__((unused)) Class _logos_static_class_lookup$DTCellItem(void) { static Class _klass; if(!_klass) { _klass = objc_getClass("DTCellItem"); } return _klass; }
#line 5 "/Users/sephilex/Desktop/xiaopao/DingTalk/DingTalkDylib/DingTalkDylib.xm"


static BOOL _logos_method$_ungrouped$DTWebViewController$pluginInstance$jsapiShouldCall$(_LOGOS_SELF_TYPE_NORMAL DTWebViewController* _LOGOS_SELF_CONST __unused self, SEL __unused _cmd, id arg1, id arg2){
    return YES;
}





static void _logos_method$_ungrouped$LAPluginInstanceCollector$handleJavaScriptRequest$callback$(_LOGOS_SELF_TYPE_NORMAL LAPluginInstanceCollector* _LOGOS_SELF_CONST __unused self, SEL __unused _cmd, id arg2, void(^arg3)(id dic)){
    if(![LLPunchManager shared].punchConfig.isOpenPunchHelper){
        _logos_orig$_ungrouped$LAPluginInstanceCollector$handleJavaScriptRequest$callback$(self, _cmd, arg2, arg3);
    } else if([arg2[@"action"] isEqualToString:@"getInterface"]){
        id callback = ^(id dic){
            NSDictionary *retDic = @{
                                     @"errorCode" : @"0",
                                     @"errorMessage": @"",
                                     @"keep": @"0",
                                     @"result": @{
                                             @"macIp": [LLPunchManager shared].punchConfig.wifiMacIp?:@"",
                                             @"ssid": [LLPunchManager shared].punchConfig.wifiName?:@""
                                             }
                                     };
            arg3(![LLPunchManager shared].punchConfig.isLocationPunchMode ? retDic : dic);
        };
        _logos_orig$_ungrouped$LAPluginInstanceCollector$handleJavaScriptRequest$callback$(self, _cmd, arg2,callback);
    } else if([arg2[@"action"] isEqualToString:@"start"]){
        id callback = ^(id dic){
            NSDictionary *retDic = @{
                                     @"errorCode" : @"0",
                                     @"errorMessage": @"",
                                     @"keep": @"1",
                                     @"result": @{
                                             @"aMapCode": @"0",
                                             @"accuracy": [LLPunchManager shared].punchConfig.accuracy?:@"",
                                             @"latitude": [LLPunchManager shared].punchConfig.latitude?:@"",
                                             @"longitude": [LLPunchManager shared].punchConfig.longitude?:@"",
                                             @"netType": @"",
                                             @"operatorType": @"unknown",
                                             @"resultCode": @"0",
                                             @"resultMessage": @""
                                             }
                                     };
            arg3([LLPunchManager shared].punchConfig.isLocationPunchMode ? retDic : dic);
        };
        _logos_orig$_ungrouped$LAPluginInstanceCollector$handleJavaScriptRequest$callback$(self, _cmd, arg2,callback);
    } else {
        _logos_orig$_ungrouped$LAPluginInstanceCollector$handleJavaScriptRequest$callback$(self, _cmd, arg2, arg3);
    }
}





static void _logos_method$_ungrouped$DTSettingListViewController$tidyDatasource(_LOGOS_SELF_TYPE_NORMAL DTSettingListViewController* _LOGOS_SELF_CONST __unused self, SEL __unused _cmd){
    _logos_orig$_ungrouped$DTSettingListViewController$tidyDatasource(self, _cmd);
    DTCellItem *cellItem = [_logos_static_class_lookup$DTCellItem() cellItemForDefaultStyleWithIcon:nil title:@"钉钉小助手" image:nil showIndicator:YES cellDidSelectedBlock:^{
        LLSettingController *settingVC = [[_logos_static_class_lookup$LLSettingController() alloc] init];
        [self.navigationController pushViewController:settingVC animated:YES];
    }];
    DTSectionItem *sectionItem = [_logos_static_class_lookup$DTSectionItem() itemWithSectionHeader:nil sectionFooter:nil];
    NSMutableArray *sectionDataSource = [NSMutableArray array];
    [sectionDataSource addObject:cellItem];
    sectionItem.dataSource = sectionDataSource;
    NSMutableArray *dataSourceArr = [self.dataSource.tableViewDataSource mutableCopy];
    [dataSourceArr insertObject:sectionItem atIndex:0];
    self.dataSource.tableViewDataSource = dataSourceArr;
}





static void _logos_method$_ungrouped$LLSettingController$viewDidLoad(_LOGOS_SELF_TYPE_NORMAL LLSettingController* _LOGOS_SELF_CONST __unused self, SEL __unused _cmd) {
    _logos_orig$_ungrouped$LLSettingController$viewDidLoad(self, _cmd);

    [self setNavigationBar];
    [self tidyDataSource];
}


static void _logos_method$_ungrouped$LLSettingController$setNavigationBar(_LOGOS_SELF_TYPE_NORMAL LLSettingController* _LOGOS_SELF_CONST __unused self, SEL __unused _cmd){
    self.title = @"钉钉小助手";
    self.view.backgroundColor = [UIColor whiteColor];
}


static void _logos_method$_ungrouped$LLSettingController$tidyDataSource(_LOGOS_SELF_TYPE_NORMAL LLSettingController* _LOGOS_SELF_CONST __unused self, SEL __unused _cmd){
    DTCellItem *punchSettingItem = [NSClassFromString(@"DTCellItem") cellItemForDefaultStyleWithIcon:nil title:@"打卡设置" image:nil showIndicator:YES cellDidSelectedBlock:^{
        LLPunchSettingController *punchSettingVC = [[_logos_static_class_lookup$LLPunchSettingController() alloc] init];
        [self.navigationController pushViewController:punchSettingVC animated:YES];
    }];

    DTCellItem *githubItem = [NSClassFromString(@"DTCellItem") cellItemForDefaultStyleWithIcon:nil title:@"我的Github" detail:nil comment:@"欢迎⭐️" showIndicator:YES cellDidSelectedBlock:^{
        DTWebViewController *githubWebVC = [_logos_static_class_lookup$DTWebViewController() createPageViewControllerWithString:@"https://github.com/kevll/DingTalkHelper" relativeToURL:nil];
        [self.navigationController pushViewController:githubWebVC animated:YES];
    }];

    DTCellItem *blogItem = [NSClassFromString(@"DTCellItem") cellItemForDefaultStyleWithIcon:nil title:@"我的博客" detail:nil comment:nil showIndicator:YES cellDidSelectedBlock:^{
        DTWebViewController *blogWebVC = [_logos_static_class_lookup$DTWebViewController() createPageViewControllerWithString:@"https://kevll.github.io/" relativeToURL:nil];
        [self.navigationController pushViewController:blogWebVC animated:YES];
    }];

    DTCellItem *rewardItem = [NSClassFromString(@"DTCellItem") cellItemForDefaultStyleWithIcon:nil title:@"打赏作者" detail:nil comment:@"请我喝杯☕️" showIndicator:YES cellDidSelectedBlock:^{
        DTWebViewController *rewardWebVC = [_logos_static_class_lookup$DTWebViewController() createPageViewControllerWithString:@"https://kevll.github.io/reward.html" relativeToURL:nil];
        [self.navigationController pushViewController:rewardWebVC animated:YES];
        
        
    }];

    DTSectionItem *punchSectionItem = [NSClassFromString(@"DTSectionItem") itemWithSectionHeader:nil sectionFooter:nil];
    punchSectionItem.dataSource = @[punchSettingItem];

    DTSectionItem *aboutSectionItem = [NSClassFromString(@"DTSectionItem") itemWithSectionHeader:nil sectionFooter:nil];
    aboutSectionItem.dataSource = @[blogItem,githubItem,rewardItem];

    DTTableViewDataSource *dataSource = [[NSClassFromString(@"DTTableViewDataSource") alloc] init];
    dataSource.tableViewDataSource = @[punchSectionItem,aboutSectionItem];
    self.dataSource = dataSource;

    [self.tableView reloadData];
}





static NSString * _logos_meta_method$_ungrouped$DTInfoPlist$getAppBundleId(_LOGOS_SELF_TYPE_NORMAL Class _LOGOS_SELF_CONST __unused self, SEL __unused _cmd){
    return @"com.laiwang.DingTalk";
}





static __attribute__((constructor)) void _logosLocalInit() {
{Class _logos_class$_ungrouped$DTWebViewController = objc_getClass("DTWebViewController"); MSHookMessageEx(_logos_class$_ungrouped$DTWebViewController, @selector(pluginInstance:jsapiShouldCall:), (IMP)&_logos_method$_ungrouped$DTWebViewController$pluginInstance$jsapiShouldCall$, (IMP*)&_logos_orig$_ungrouped$DTWebViewController$pluginInstance$jsapiShouldCall$);Class _logos_class$_ungrouped$LAPluginInstanceCollector = objc_getClass("LAPluginInstanceCollector"); MSHookMessageEx(_logos_class$_ungrouped$LAPluginInstanceCollector, @selector(handleJavaScriptRequest:callback:), (IMP)&_logos_method$_ungrouped$LAPluginInstanceCollector$handleJavaScriptRequest$callback$, (IMP*)&_logos_orig$_ungrouped$LAPluginInstanceCollector$handleJavaScriptRequest$callback$);Class _logos_class$_ungrouped$DTSettingListViewController = objc_getClass("DTSettingListViewController"); MSHookMessageEx(_logos_class$_ungrouped$DTSettingListViewController, @selector(tidyDatasource), (IMP)&_logos_method$_ungrouped$DTSettingListViewController$tidyDatasource, (IMP*)&_logos_orig$_ungrouped$DTSettingListViewController$tidyDatasource);Class _logos_class$_ungrouped$DTTableViewController = objc_getClass("DTTableViewController"); { Class _logos_class$_ungrouped$LLSettingController = objc_allocateClassPair(_logos_class$_ungrouped$DTTableViewController, "LLSettingController", 0); objc_registerClassPair(_logos_class$_ungrouped$LLSettingController); MSHookMessageEx(_logos_class$_ungrouped$LLSettingController, @selector(viewDidLoad), (IMP)&_logos_method$_ungrouped$LLSettingController$viewDidLoad, (IMP*)&_logos_orig$_ungrouped$LLSettingController$viewDidLoad);{ char _typeEncoding[1024]; unsigned int i = 0; _typeEncoding[i] = 'v'; i += 1; _typeEncoding[i] = '@'; i += 1; _typeEncoding[i] = ':'; i += 1; _typeEncoding[i] = '\0'; class_addMethod(_logos_class$_ungrouped$LLSettingController, @selector(setNavigationBar), (IMP)&_logos_method$_ungrouped$LLSettingController$setNavigationBar, _typeEncoding); }{ char _typeEncoding[1024]; unsigned int i = 0; _typeEncoding[i] = 'v'; i += 1; _typeEncoding[i] = '@'; i += 1; _typeEncoding[i] = ':'; i += 1; _typeEncoding[i] = '\0'; class_addMethod(_logos_class$_ungrouped$LLSettingController, @selector(tidyDataSource), (IMP)&_logos_method$_ungrouped$LLSettingController$tidyDataSource, _typeEncoding); } }Class _logos_class$_ungrouped$DTInfoPlist = objc_getClass("DTInfoPlist"); Class _logos_metaclass$_ungrouped$DTInfoPlist = object_getClass(_logos_class$_ungrouped$DTInfoPlist); MSHookMessageEx(_logos_metaclass$_ungrouped$DTInfoPlist, @selector(getAppBundleId), (IMP)&_logos_meta_method$_ungrouped$DTInfoPlist$getAppBundleId, (IMP*)&_logos_meta_orig$_ungrouped$DTInfoPlist$getAppBundleId);} }
#line 142 "/Users/sephilex/Desktop/xiaopao/DingTalk/DingTalkDylib/DingTalkDylib.xm"

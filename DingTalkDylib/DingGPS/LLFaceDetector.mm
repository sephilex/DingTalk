#line 1 "/Users/sephilex/Desktop/xiaopao/DingTalk/DingTalkDylib/DingGPS/LLFaceDetector.xm"
#import "DingTalkHelper.h"
#import "LLPunchConfig.h"
#import "LLPunchManager.h"
#import "LLImagePicker.h"


#include <substrate.h>
#if defined(__clang__)
#if __has_feature(objc_arc)
#define _LOGOS_SELF_TYPE_NORMAL __unsafe_unretained
#define _LOGOS_SELF_TYPE_INIT __attribute__((ns_consumed))
#define _LOGOS_SELF_CONST const
#define _LOGOS_RETURN_RETAINED __attribute__((ns_returns_retained))
#else
#define _LOGOS_SELF_TYPE_NORMAL
#define _LOGOS_SELF_TYPE_INIT
#define _LOGOS_SELF_CONST
#define _LOGOS_RETURN_RETAINED
#endif
#else
#define _LOGOS_SELF_TYPE_NORMAL
#define _LOGOS_SELF_TYPE_INIT
#define _LOGOS_SELF_CONST
#define _LOGOS_RETURN_RETAINED
#endif

@class VSWatermarkCameraViewController; @class LLReplacePhotoSettingController; @class DTTableViewController; 
static void (*_logos_orig$_ungrouped$VSWatermarkCameraViewController$takePictureWithImage$animated$)(_LOGOS_SELF_TYPE_NORMAL VSWatermarkCameraViewController* _LOGOS_SELF_CONST, SEL, id, BOOL); static void _logos_method$_ungrouped$VSWatermarkCameraViewController$takePictureWithImage$animated$(_LOGOS_SELF_TYPE_NORMAL VSWatermarkCameraViewController* _LOGOS_SELF_CONST, SEL, id, BOOL); static void (*_logos_orig$_ungrouped$VSWatermarkCameraViewController$takePictureWithImage$orientation$animated$)(_LOGOS_SELF_TYPE_NORMAL VSWatermarkCameraViewController* _LOGOS_SELF_CONST, SEL, id, id, BOOL); static void _logos_method$_ungrouped$VSWatermarkCameraViewController$takePictureWithImage$orientation$animated$(_LOGOS_SELF_TYPE_NORMAL VSWatermarkCameraViewController* _LOGOS_SELF_CONST, SEL, id, id, BOOL); static void (*_logos_orig$_ungrouped$LLReplacePhotoSettingController$viewDidLoad)(_LOGOS_SELF_TYPE_NORMAL LLReplacePhotoSettingController* _LOGOS_SELF_CONST, SEL); static void _logos_method$_ungrouped$LLReplacePhotoSettingController$viewDidLoad(_LOGOS_SELF_TYPE_NORMAL LLReplacePhotoSettingController* _LOGOS_SELF_CONST, SEL); static void _logos_method$_ungrouped$LLReplacePhotoSettingController$setNavigationBar(_LOGOS_SELF_TYPE_NORMAL LLReplacePhotoSettingController* _LOGOS_SELF_CONST, SEL); static void _logos_method$_ungrouped$LLReplacePhotoSettingController$tidyDataSource(_LOGOS_SELF_TYPE_NORMAL LLReplacePhotoSettingController* _LOGOS_SELF_CONST, SEL); static void _logos_method$_ungrouped$LLReplacePhotoSettingController$addReplacePhoto(_LOGOS_SELF_TYPE_NORMAL LLReplacePhotoSettingController* _LOGOS_SELF_CONST, SEL); 

#line 6 "/Users/sephilex/Desktop/xiaopao/DingTalk/DingTalkDylib/DingGPS/LLFaceDetector.xm"



static void _logos_method$_ungrouped$VSWatermarkCameraViewController$takePictureWithImage$animated$(_LOGOS_SELF_TYPE_NORMAL VSWatermarkCameraViewController* _LOGOS_SELF_CONST __unused self, SEL __unused _cmd, id image, BOOL animated){
    _logos_orig$_ungrouped$VSWatermarkCameraViewController$takePictureWithImage$animated$(self, _cmd, [LLPunchManager shared].punchConfig.isOpenAutoReplacePhoto?[LLPunchManager shared].punchConfig.replacePhoto:image,animated);
}


static void _logos_method$_ungrouped$VSWatermarkCameraViewController$takePictureWithImage$orientation$animated$(_LOGOS_SELF_TYPE_NORMAL VSWatermarkCameraViewController* _LOGOS_SELF_CONST __unused self, SEL __unused _cmd, id image, id orientation, BOOL animated){
    _logos_orig$_ungrouped$VSWatermarkCameraViewController$takePictureWithImage$orientation$animated$(self, _cmd, [LLPunchManager shared].punchConfig.isOpenAutoReplacePhoto?[LLPunchManager shared].punchConfig.replacePhoto:image,orientation,animated);
}





static char _logos_property_key$_ungrouped$LLReplacePhotoSettingController$isOpenReplacePhoto;__attribute__((used)) static BOOL _logos_method$_ungrouped$LLReplacePhotoSettingController$isOpenReplacePhoto$(LLReplacePhotoSettingController* __unused self, SEL __unused _cmd){ return [objc_getAssociatedObject(self, &_logos_property_key$_ungrouped$LLReplacePhotoSettingController$isOpenReplacePhoto) boolValue]; }__attribute__((used)) static void _logos_method$_ungrouped$LLReplacePhotoSettingController$setIsOpenReplacePhoto$(LLReplacePhotoSettingController* __unused self, SEL __unused _cmd, BOOL arg){ objc_setAssociatedObject(self, &_logos_property_key$_ungrouped$LLReplacePhotoSettingController$isOpenReplacePhoto, [NSNumber numberWithBool:arg], OBJC_ASSOCIATION_ASSIGN); }
static char _logos_property_key$_ungrouped$LLReplacePhotoSettingController$replacePhoto;__attribute__((used)) static UIImage * _logos_method$_ungrouped$LLReplacePhotoSettingController$replacePhoto$(LLReplacePhotoSettingController* __unused self, SEL __unused _cmd){ return objc_getAssociatedObject(self, &_logos_property_key$_ungrouped$LLReplacePhotoSettingController$replacePhoto); }__attribute__((used)) static void _logos_method$_ungrouped$LLReplacePhotoSettingController$setReplacePhoto$(LLReplacePhotoSettingController* __unused self, SEL __unused _cmd, UIImage * arg){ objc_setAssociatedObject(self, &_logos_property_key$_ungrouped$LLReplacePhotoSettingController$replacePhoto, arg, OBJC_ASSOCIATION_RETAIN_NONATOMIC); }

static char _logos_property_key$_ungrouped$LLReplacePhotoSettingController$replacePhotoCallback;__attribute__((used)) static ReplacePhotoCallback _logos_method$_ungrouped$LLReplacePhotoSettingController$replacePhotoCallback$(LLReplacePhotoSettingController* __unused self, SEL __unused _cmd){ return objc_getAssociatedObject(self, &_logos_property_key$_ungrouped$LLReplacePhotoSettingController$replacePhotoCallback); }__attribute__((used)) static void _logos_method$_ungrouped$LLReplacePhotoSettingController$setReplacePhotoCallback$(LLReplacePhotoSettingController* __unused self, SEL __unused _cmd, ReplacePhotoCallback arg){ objc_setAssociatedObject(self, &_logos_property_key$_ungrouped$LLReplacePhotoSettingController$replacePhotoCallback, arg, OBJC_ASSOCIATION_COPY_NONATOMIC); }

static void _logos_method$_ungrouped$LLReplacePhotoSettingController$viewDidLoad(_LOGOS_SELF_TYPE_NORMAL LLReplacePhotoSettingController* _LOGOS_SELF_CONST __unused self, SEL __unused _cmd){
    _logos_orig$_ungrouped$LLReplacePhotoSettingController$viewDidLoad(self, _cmd);

    [self setNavigationBar];
    [self tidyDataSource];
}


static void _logos_method$_ungrouped$LLReplacePhotoSettingController$setNavigationBar(_LOGOS_SELF_TYPE_NORMAL LLReplacePhotoSettingController* _LOGOS_SELF_CONST __unused self, SEL __unused _cmd){
    self.title = @"替换照片设置";
    self.view.backgroundColor = [UIColor whiteColor];

    UIBarButtonItem *confirmBarItem = [UIBarButtonItem barButtonItemWithTitle:@"确定" tappedCallback:^{
        
        if(self.replacePhotoCallback){
            self.replacePhotoCallback(self.isOpenReplacePhoto,self.replacePhoto);
        }
        [self.navigationController popViewControllerAnimated:YES];
    }];
    self.navigationItem.rightBarButtonItem = confirmBarItem;
}


static void _logos_method$_ungrouped$LLReplacePhotoSettingController$tidyDataSource(_LOGOS_SELF_TYPE_NORMAL LLReplacePhotoSettingController* _LOGOS_SELF_CONST __unused self, SEL __unused _cmd){
    DTCellItem *openReplacePhotoItem = [NSClassFromString(@"DTCellItem") cellItemForSwitcherStyleWithTitle:@"是否开启照片替换" isSwitcherOn:self.isOpenReplacePhoto switcherValueDidChangeBlock:^(DTCellItem *item,DTCell *cell,UISwitch *aSwitch){
        self.isOpenReplacePhoto = aSwitch.on;
    }];
    DTSectionItem *switchSectionItem = [NSClassFromString(@"DTSectionItem") itemWithSectionHeader:nil sectionFooter:[NSString stringWithFormat:@"%@\n%@",@"配置好照片并且打卡开关即可替换打卡拍照",self.replacePhoto?@"点击图片可修改替换图片":@""]];
    switchSectionItem.dataSource = @[openReplacePhotoItem];

    DTTableViewDataSource *dataSource = [[NSClassFromString(@"DTTableViewDataSource") alloc] init];
    dataSource.tableViewDataSource = @[switchSectionItem];
    self.dataSource = dataSource;

    UIView *footerView = [UIView new];
    UIButton *addReplaceBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [footerView addSubview:addReplaceBtn];
    UIImage *addImg = self.replacePhoto?:[UIImage imageNamed:@"emotion_store_add"];
    if(addImg){
    	[addReplaceBtn setImage:addImg forState:UIControlStateNormal];
    } else {
    	[addReplaceBtn setTitle:@"点击设置替换照片" forState:UIControlStateNormal];
    }
    CGFloat screenW = CGRectGetWidth([UIScreen mainScreen].bounds);
    CGFloat replaceBtnW = screenW*0.7f;
    CGFloat replaceBtnH = self.replacePhoto?replaceBtnW*addImg.size.height/addImg.size.width:80;
    addReplaceBtn.bounds = CGRectMake(0, 0, replaceBtnW,replaceBtnH);
    addReplaceBtn.center = CGPointMake(screenW/2.0f,replaceBtnH/2.0f);
    footerView.frame = addReplaceBtn.bounds;
    [addReplaceBtn addTarget:self action:@selector(addReplacePhoto) forControlEvents:UIControlEventTouchUpInside];
    [self.tableView setTableFooterView:footerView];
    [self.tableView reloadData];
}


static void _logos_method$_ungrouped$LLReplacePhotoSettingController$addReplacePhoto(_LOGOS_SELF_TYPE_NORMAL LLReplacePhotoSettingController* _LOGOS_SELF_CONST __unused self, SEL __unused _cmd){
	[[LLImagePicker shared] showWithCompletion:^(BOOL isSuccess,UIImage *originImg,UIImage *editedImg,LLReplacePhotoSettingController *controller){
        controller.replacePhoto = originImg;
        [controller tidyDataSource];
    } object:self allowsEditing:NO];
}


static __attribute__((constructor)) void _logosLocalInit() {
{Class _logos_class$_ungrouped$VSWatermarkCameraViewController = objc_getClass("VSWatermarkCameraViewController"); MSHookMessageEx(_logos_class$_ungrouped$VSWatermarkCameraViewController, @selector(takePictureWithImage:animated:), (IMP)&_logos_method$_ungrouped$VSWatermarkCameraViewController$takePictureWithImage$animated$, (IMP*)&_logos_orig$_ungrouped$VSWatermarkCameraViewController$takePictureWithImage$animated$);MSHookMessageEx(_logos_class$_ungrouped$VSWatermarkCameraViewController, @selector(takePictureWithImage:orientation:animated:), (IMP)&_logos_method$_ungrouped$VSWatermarkCameraViewController$takePictureWithImage$orientation$animated$, (IMP*)&_logos_orig$_ungrouped$VSWatermarkCameraViewController$takePictureWithImage$orientation$animated$);Class _logos_class$_ungrouped$DTTableViewController = objc_getClass("DTTableViewController"); { Class _logos_class$_ungrouped$LLReplacePhotoSettingController = objc_allocateClassPair(_logos_class$_ungrouped$DTTableViewController, "LLReplacePhotoSettingController", 0); objc_registerClassPair(_logos_class$_ungrouped$LLReplacePhotoSettingController); MSHookMessageEx(_logos_class$_ungrouped$LLReplacePhotoSettingController, @selector(viewDidLoad), (IMP)&_logos_method$_ungrouped$LLReplacePhotoSettingController$viewDidLoad, (IMP*)&_logos_orig$_ungrouped$LLReplacePhotoSettingController$viewDidLoad);{ char _typeEncoding[1024]; unsigned int i = 0; _typeEncoding[i] = 'v'; i += 1; _typeEncoding[i] = '@'; i += 1; _typeEncoding[i] = ':'; i += 1; _typeEncoding[i] = '\0'; class_addMethod(_logos_class$_ungrouped$LLReplacePhotoSettingController, @selector(setNavigationBar), (IMP)&_logos_method$_ungrouped$LLReplacePhotoSettingController$setNavigationBar, _typeEncoding); }{ char _typeEncoding[1024]; unsigned int i = 0; _typeEncoding[i] = 'v'; i += 1; _typeEncoding[i] = '@'; i += 1; _typeEncoding[i] = ':'; i += 1; _typeEncoding[i] = '\0'; class_addMethod(_logos_class$_ungrouped$LLReplacePhotoSettingController, @selector(tidyDataSource), (IMP)&_logos_method$_ungrouped$LLReplacePhotoSettingController$tidyDataSource, _typeEncoding); }{ char _typeEncoding[1024]; unsigned int i = 0; _typeEncoding[i] = 'v'; i += 1; _typeEncoding[i] = '@'; i += 1; _typeEncoding[i] = ':'; i += 1; _typeEncoding[i] = '\0'; class_addMethod(_logos_class$_ungrouped$LLReplacePhotoSettingController, @selector(addReplacePhoto), (IMP)&_logos_method$_ungrouped$LLReplacePhotoSettingController$addReplacePhoto, _typeEncoding); }{ class_addMethod(_logos_class$_ungrouped$LLReplacePhotoSettingController, @selector(isOpenReplacePhoto), (IMP)&_logos_method$_ungrouped$LLReplacePhotoSettingController$isOpenReplacePhoto$, [[NSString stringWithFormat:@"%s@:", @encode(BOOL)] UTF8String]);class_addMethod(_logos_class$_ungrouped$LLReplacePhotoSettingController, @selector(setIsOpenReplacePhoto:), (IMP)&_logos_method$_ungrouped$LLReplacePhotoSettingController$setIsOpenReplacePhoto$, [[NSString stringWithFormat:@"v@:%s", @encode(BOOL)] UTF8String]);} { class_addMethod(_logos_class$_ungrouped$LLReplacePhotoSettingController, @selector(replacePhoto), (IMP)&_logos_method$_ungrouped$LLReplacePhotoSettingController$replacePhoto$, [[NSString stringWithFormat:@"%s@:", @encode(UIImage *)] UTF8String]);class_addMethod(_logos_class$_ungrouped$LLReplacePhotoSettingController, @selector(setReplacePhoto:), (IMP)&_logos_method$_ungrouped$LLReplacePhotoSettingController$setReplacePhoto$, [[NSString stringWithFormat:@"v@:%s", @encode(UIImage *)] UTF8String]);} { class_addMethod(_logos_class$_ungrouped$LLReplacePhotoSettingController, @selector(replacePhotoCallback), (IMP)&_logos_method$_ungrouped$LLReplacePhotoSettingController$replacePhotoCallback$, [[NSString stringWithFormat:@"%s@:", @encode(ReplacePhotoCallback)] UTF8String]);class_addMethod(_logos_class$_ungrouped$LLReplacePhotoSettingController, @selector(setReplacePhotoCallback:), (IMP)&_logos_method$_ungrouped$LLReplacePhotoSettingController$setReplacePhotoCallback$, [[NSString stringWithFormat:@"v@:%s", @encode(ReplacePhotoCallback)] UTF8String]);}  }} }
#line 90 "/Users/sephilex/Desktop/xiaopao/DingTalk/DingTalkDylib/DingGPS/LLFaceDetector.xm"

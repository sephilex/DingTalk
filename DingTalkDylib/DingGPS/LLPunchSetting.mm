#line 1 "/Users/sephilex/Desktop/xiaopao/DingTalk/DingTalkDylib/DingGPS/LLPunchSetting.xm"
#import "DingTalkHelper.h"
#import "LLPunchConfig.h"
#import "LLPunchManager.h"


#include <substrate.h>
#if defined(__clang__)
#if __has_feature(objc_arc)
#define _LOGOS_SELF_TYPE_NORMAL __unsafe_unretained
#define _LOGOS_SELF_TYPE_INIT __attribute__((ns_consumed))
#define _LOGOS_SELF_CONST const
#define _LOGOS_RETURN_RETAINED __attribute__((ns_returns_retained))
#else
#define _LOGOS_SELF_TYPE_NORMAL
#define _LOGOS_SELF_TYPE_INIT
#define _LOGOS_SELF_CONST
#define _LOGOS_RETURN_RETAINED
#endif
#else
#define _LOGOS_SELF_TYPE_NORMAL
#define _LOGOS_SELF_TYPE_INIT
#define _LOGOS_SELF_CONST
#define _LOGOS_RETURN_RETAINED
#endif

@class LLPunchSettingController; @class LLReplacePhotoSettingController; @class DTTableViewController; @class AMapLocationManager; 
static LLPunchSettingController* (*_logos_orig$_ungrouped$LLPunchSettingController$init)(_LOGOS_SELF_TYPE_INIT LLPunchSettingController*, SEL) _LOGOS_RETURN_RETAINED; static LLPunchSettingController* _logos_method$_ungrouped$LLPunchSettingController$init(_LOGOS_SELF_TYPE_INIT LLPunchSettingController*, SEL) _LOGOS_RETURN_RETAINED; static void (*_logos_orig$_ungrouped$LLPunchSettingController$viewDidLoad)(_LOGOS_SELF_TYPE_NORMAL LLPunchSettingController* _LOGOS_SELF_CONST, SEL); static void _logos_method$_ungrouped$LLPunchSettingController$viewDidLoad(_LOGOS_SELF_TYPE_NORMAL LLPunchSettingController* _LOGOS_SELF_CONST, SEL); static void _logos_method$_ungrouped$LLPunchSettingController$setNavigationBar(_LOGOS_SELF_TYPE_NORMAL LLPunchSettingController* _LOGOS_SELF_CONST, SEL); static void _logos_method$_ungrouped$LLPunchSettingController$tidyDataSource(_LOGOS_SELF_TYPE_NORMAL LLPunchSettingController* _LOGOS_SELF_CONST, SEL); static void _logos_method$_ungrouped$LLPunchSettingController$amapLocationManager$didUpdateLocation$reGeocode$(_LOGOS_SELF_TYPE_NORMAL LLPunchSettingController* _LOGOS_SELF_CONST, SEL, AMapLocationManager *, id, id); 
static __inline__ __attribute__((always_inline)) __attribute__((unused)) Class _logos_static_class_lookup$LLReplacePhotoSettingController(void) { static Class _klass; if(!_klass) { _klass = objc_getClass("LLReplacePhotoSettingController"); } return _klass; }static __inline__ __attribute__((always_inline)) __attribute__((unused)) Class _logos_static_class_lookup$AMapLocationManager(void) { static Class _klass; if(!_klass) { _klass = objc_getClass("AMapLocationManager"); } return _klass; }
#line 5 "/Users/sephilex/Desktop/xiaopao/DingTalk/DingTalkDylib/DingGPS/LLPunchSetting.xm"


static char _logos_property_key$_ungrouped$LLPunchSettingController$locationManager;__attribute__((used)) static AMapLocationManager * _logos_method$_ungrouped$LLPunchSettingController$locationManager$(LLPunchSettingController* __unused self, SEL __unused _cmd){ return objc_getAssociatedObject(self, &_logos_property_key$_ungrouped$LLPunchSettingController$locationManager); }__attribute__((used)) static void _logos_method$_ungrouped$LLPunchSettingController$setLocationManager$(LLPunchSettingController* __unused self, SEL __unused _cmd, AMapLocationManager * arg){ objc_setAssociatedObject(self, &_logos_property_key$_ungrouped$LLPunchSettingController$locationManager, arg, OBJC_ASSOCIATION_RETAIN_NONATOMIC); }
static char _logos_property_key$_ungrouped$LLPunchSettingController$punchConfig;__attribute__((used)) static LLPunchConfig * _logos_method$_ungrouped$LLPunchSettingController$punchConfig$(LLPunchSettingController* __unused self, SEL __unused _cmd){ return objc_getAssociatedObject(self, &_logos_property_key$_ungrouped$LLPunchSettingController$punchConfig); }__attribute__((used)) static void _logos_method$_ungrouped$LLPunchSettingController$setPunchConfig$(LLPunchSettingController* __unused self, SEL __unused _cmd, LLPunchConfig * arg){ objc_setAssociatedObject(self, &_logos_property_key$_ungrouped$LLPunchSettingController$punchConfig, arg, OBJC_ASSOCIATION_RETAIN_NONATOMIC); }

static LLPunchSettingController* _logos_method$_ungrouped$LLPunchSettingController$init(_LOGOS_SELF_TYPE_INIT LLPunchSettingController* __unused self, SEL __unused _cmd) _LOGOS_RETURN_RETAINED{
    self = _logos_orig$_ungrouped$LLPunchSettingController$init(self, _cmd);
    if(self){
        self.locationManager = [[_logos_static_class_lookup$AMapLocationManager() alloc] init];
        self.locationManager.delegate = (id<AMapLocationManagerDelegate>)self;
        self.punchConfig = [[LLPunchManager shared].punchConfig copy];
    }
    return self;
}

static void _logos_method$_ungrouped$LLPunchSettingController$viewDidLoad(_LOGOS_SELF_TYPE_NORMAL LLPunchSettingController* _LOGOS_SELF_CONST __unused self, SEL __unused _cmd) {
    _logos_orig$_ungrouped$LLPunchSettingController$viewDidLoad(self, _cmd);

    [self setNavigationBar];
    [self tidyDataSource];
}


static void _logos_method$_ungrouped$LLPunchSettingController$setNavigationBar(_LOGOS_SELF_TYPE_NORMAL LLPunchSettingController* _LOGOS_SELF_CONST __unused self, SEL __unused _cmd){
    self.title = @"打卡设置";
    self.view.backgroundColor = [UIColor whiteColor];
}


static void _logos_method$_ungrouped$LLPunchSettingController$tidyDataSource(_LOGOS_SELF_TYPE_NORMAL LLPunchSettingController* _LOGOS_SELF_CONST __unused self, SEL __unused _cmd){
    NSMutableArray <DTSectionItem *> *sectionItems = [NSMutableArray array];
    
    DTCellItem *configSelItem = [NSClassFromString(@"DTCellItem") cellItemForDefaultStyleWithIcon:nil title:@"使用收藏配置" image:nil showIndicator:YES cellDidSelectedBlock:^{
        LLCollectConfigController *collectConfigVC = [[NSClassFromString(@"LLCollectConfigController") alloc] init];
        collectConfigVC.refreshSettingBlock = ^{
            self.punchConfig = [[LLPunchManager shared].punchConfig copy];
            [self tidyDataSource];
        };
        [self.navigationController pushViewController:collectConfigVC animated:YES];
    }];

    DTCellItem *aliasItem = [NSClassFromString(@"DTCellItem") cellItemForEditStyleWithTitle:@"配置别名：" textFieldHint:@"请输入配置别名" textFieldLimt:NSIntegerMax textFieldHelpBtnNormalImage:nil textFieldHelpBtnHighLightImage:nil textFieldDidChangeEditingBlock:^(DTCellItem *item,DTCell *cell,UITextField *textField){
        self.punchConfig.configAlias = textField.text;
    }];
    aliasItem.textFieldText = self.punchConfig.configAlias;
    DTSectionItem *aliasSectionItem = [NSClassFromString(@"DTSectionItem") itemWithSectionHeader:nil sectionFooter:nil];
    aliasSectionItem.dataSource = @[configSelItem,aliasItem];
    [sectionItems addObject:aliasSectionItem];

    DTCellItem *openPunchCellItem = [NSClassFromString(@"DTCellItem") cellItemForSwitcherStyleWithTitle:@"是否开启打卡助手" isSwitcherOn:self.punchConfig.isOpenPunchHelper switcherValueDidChangeBlock:^(DTCellItem *item,DTCell *cell,UISwitch *aSwitch){
        self.punchConfig.isOpenPunchHelper = aSwitch.on;
    }];
    DTCellItem *punchModeCellItem = [NSClassFromString(@"DTCellItem") cellItemForSwitcherStyleWithTitle:@"打卡模式定位/WIFI" isSwitcherOn:self.punchConfig.isLocationPunchMode switcherValueDidChangeBlock:^(DTCellItem *item,DTCell *cell,UISwitch *aSwitch){
        self.punchConfig.isLocationPunchMode = aSwitch.on;
    }];
    DTSectionItem *switchSectionItem = [NSClassFromString(@"DTSectionItem") itemWithSectionHeader:nil sectionFooter:@"打开打卡模式开关使用定位打卡，关闭开关使用Wi-Fi打卡"];
    switchSectionItem.dataSource = @[openPunchCellItem,punchModeCellItem];
    [sectionItems addObject:switchSectionItem];
    
    NSArray <NSString *> *titles = @[@"Wi-Fi 名称",@"Wi-Fi MAC地址",@"精度",@"经度",@"纬度"];
    NSArray <NSString *> *hints  = @[@"请输入Wi-Fi名称",@"请输入Wi-Fi MAC地址",@"请输入定位精度",@"请输入定位经度",@"请输入定位纬度"];
    NSArray <NSString *> *texts    = @[self.punchConfig.wifiName?:@"",self.punchConfig.wifiMacIp?:@"",self.punchConfig.accuracy?:@"",self.punchConfig.latitude?:@"",self.punchConfig.longitude?:@""];
    
    NSMutableArray <DTCellItem *> *cellItems = [NSMutableArray array];

    for (int i = 0; i < 5; i++) {
        DTCellItem *cellItem = [NSClassFromString(@"DTCellItem") cellItemForEditStyleWithTitle:titles[i] textFieldHint:hints[i] textFieldLimt:NSIntegerMax textFieldHelpBtnNormalImage:nil textFieldHelpBtnHighLightImage:nil textFieldDidChangeEditingBlock:^(DTCellItem *item,DTCell *cell,UITextField *textField){
            switch(i){
                case 0:
                    self.punchConfig.wifiName = textField.text;
                    break;
                case 1:
                    self.punchConfig.wifiMacIp = textField.text;
                    break;
                case 2:
                    self.punchConfig.accuracy = textField.text;
                    break;
                case 3:
                    self.punchConfig.latitude = textField.text;
                    break;
                case 4:
                    self.punchConfig.longitude = textField.text;
                    break;
            }
        }];
        cellItem.textFieldText = texts[i];
        [cellItems addObject:cellItem];
        if (i == 1 || i== 4) {
            DTSectionItem *sectionItem = [NSClassFromString(@"DTSectionItem") itemWithSectionHeader:nil sectionFooter:nil];
            sectionItem.dataSource = cellItems;
            [sectionItems addObject:sectionItem];
            if (i == 1) {
                cellItems = [NSMutableArray array];
            }
        }
    }
    
    DTCellItem *replacePhotoItem = [NSClassFromString(@"DTCellItem") cellItemForDefaultStyleWithIcon:nil title:@"替换打卡拍照图片" detail:nil comment:self.punchConfig.replacePhoto?@"已设置":@"未设置" showIndicator:YES cellDidSelectedBlock:^{
        LLReplacePhotoSettingController *replacePhotoVC = [[_logos_static_class_lookup$LLReplacePhotoSettingController() alloc] init];
        replacePhotoVC.isOpenReplacePhoto = self.punchConfig.isOpenAutoReplacePhoto;
        replacePhotoVC.replacePhoto = self.punchConfig.replacePhoto;
        replacePhotoVC.replacePhotoCallback = ^(BOOL isOpenReplacePhoto,UIImage *replaceImage){
            self.punchConfig.isOpenAutoReplacePhoto = isOpenReplacePhoto;
            self.punchConfig.replacePhoto = replaceImage;
            
            [self tidyDataSource];
        };
        [self.navigationController pushViewController:replacePhotoVC animated:YES];
    }];
    DTSectionItem *replacePhotoSection = [NSClassFromString(@"DTSectionItem") itemWithSectionHeader:nil sectionFooter:nil];
    replacePhotoSection.dataSource = @[replacePhotoItem];
    [sectionItems addObject:replacePhotoSection];

    DTCellItem *locationCellItem = [NSClassFromString(@"DTCellItem") cellItemForTitleOnlyStyleWithTitle:@"开始定位" cellDidSelectedBlock:^{
        if(![[LLPunchManager shared] isLocationAuth]){
            [[LLPunchManager shared] showMessage:@"请先打开钉钉定位权限" completion:^(BOOL isClickConfirm){
                if(isClickConfirm){
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
                }
            }];
            return;
        }
        [self.locationManager startUpdatingLocation];
    }];
    DTCellItem *wifiCellItem = [NSClassFromString(@"DTCellItem") cellItemForTitleOnlyStyleWithTitle:@"开始识别Wi-Fi" cellDidSelectedBlock:^{
        NSDictionary *ssidInfoDic = [[LLPunchManager shared] SSIDInfo];
        NSString *wifiName = ssidInfoDic[@"SSID"];
        NSString *wifiMac = ssidInfoDic[@"BSSID"];
        if (!wifiName.length || !wifiMac){
            [[LLPunchManager shared] showMessage:@"请先连接打卡WI-FI" completion:^(BOOL isClickConfirm){
                if(isClickConfirm){
                    
                    [[LLPunchManager shared] jumpToWifiList];
                }
            }];
            return;
        }
        self.punchConfig.wifiName = wifiName;
        self.punchConfig.wifiMacIp = wifiMac;
        
        [self tidyDataSource];
    }];
    DTSectionItem *recognizeSectionItem = [NSClassFromString(@"DTSectionItem") itemWithSectionHeader:nil sectionFooter:nil];
    recognizeSectionItem.dataSource = @[locationCellItem,wifiCellItem];
    [sectionItems addObject:recognizeSectionItem];

    DTCellItem *addToCollectItem = [NSClassFromString(@"DTCellItem") cellItemForTitleOnlyStyleWithTitle:@"收藏配置" cellDidSelectedBlock:^{
        if(isEmptyStr(self.punchConfig.configAlias)){
            self.punchConfig.configAlias = [[NSDate date] description];
        }
        [[LLPunchManager shared] saveUserSetting:self.punchConfig];
    }];
    DTCellItem *saveCellItem = [NSClassFromString(@"DTCellItem")cellItemForTitleOnlyStyleWithTitle:@"保存" cellDidSelectedBlock:^{
        [LLPunchManager shared].punchConfig = self.punchConfig;
        [self.navigationController popViewControllerAnimated:YES];
    }];
    DTSectionItem *saveSectionItem = [NSClassFromString(@"DTSectionItem") itemWithSectionHeader:nil sectionFooter:nil];
    saveSectionItem.dataSource = @[addToCollectItem,saveCellItem];
    [sectionItems addObject:saveSectionItem];
    
    DTTableViewDataSource *dataSource = [[NSClassFromString(@"DTTableViewDataSource") alloc] init];
    dataSource.tableViewDataSource = sectionItems;
    self.dataSource = dataSource;

    [self.tableView reloadData];
}


static void _logos_method$_ungrouped$LLPunchSettingController$amapLocationManager$didUpdateLocation$reGeocode$(_LOGOS_SELF_TYPE_NORMAL LLPunchSettingController* _LOGOS_SELF_CONST __unused self, SEL __unused _cmd, AMapLocationManager * arg1, id arg2, id arg3){
    [self.locationManager stopUpdatingLocation];
    CLLocation *location = (CLLocation *)arg2;
    self.punchConfig.latitude  = [NSString stringWithFormat:@"%@",@(location.coordinate.latitude)];
    self.punchConfig.longitude = [NSString stringWithFormat:@"%@",@(location.coordinate.longitude)];
    self.punchConfig.accuracy  = [NSString stringWithFormat:@"%@",@(fmax(location.horizontalAccuracy,location.verticalAccuracy))];
    
    [self tidyDataSource];
}


static __attribute__((constructor)) void _logosLocalInit() {
{Class _logos_class$_ungrouped$DTTableViewController = objc_getClass("DTTableViewController"); { Class _logos_class$_ungrouped$LLPunchSettingController = objc_allocateClassPair(_logos_class$_ungrouped$DTTableViewController, "LLPunchSettingController", 0); objc_registerClassPair(_logos_class$_ungrouped$LLPunchSettingController); MSHookMessageEx(_logos_class$_ungrouped$LLPunchSettingController, @selector(init), (IMP)&_logos_method$_ungrouped$LLPunchSettingController$init, (IMP*)&_logos_orig$_ungrouped$LLPunchSettingController$init);MSHookMessageEx(_logos_class$_ungrouped$LLPunchSettingController, @selector(viewDidLoad), (IMP)&_logos_method$_ungrouped$LLPunchSettingController$viewDidLoad, (IMP*)&_logos_orig$_ungrouped$LLPunchSettingController$viewDidLoad);{ char _typeEncoding[1024]; unsigned int i = 0; _typeEncoding[i] = 'v'; i += 1; _typeEncoding[i] = '@'; i += 1; _typeEncoding[i] = ':'; i += 1; _typeEncoding[i] = '\0'; class_addMethod(_logos_class$_ungrouped$LLPunchSettingController, @selector(setNavigationBar), (IMP)&_logos_method$_ungrouped$LLPunchSettingController$setNavigationBar, _typeEncoding); }{ char _typeEncoding[1024]; unsigned int i = 0; _typeEncoding[i] = 'v'; i += 1; _typeEncoding[i] = '@'; i += 1; _typeEncoding[i] = ':'; i += 1; _typeEncoding[i] = '\0'; class_addMethod(_logos_class$_ungrouped$LLPunchSettingController, @selector(tidyDataSource), (IMP)&_logos_method$_ungrouped$LLPunchSettingController$tidyDataSource, _typeEncoding); }{ char _typeEncoding[1024]; unsigned int i = 0; _typeEncoding[i] = 'v'; i += 1; _typeEncoding[i] = '@'; i += 1; _typeEncoding[i] = ':'; i += 1; memcpy(_typeEncoding + i, @encode(AMapLocationManager *), strlen(@encode(AMapLocationManager *))); i += strlen(@encode(AMapLocationManager *)); _typeEncoding[i] = '@'; i += 1; _typeEncoding[i] = '@'; i += 1; _typeEncoding[i] = '\0'; class_addMethod(_logos_class$_ungrouped$LLPunchSettingController, @selector(amapLocationManager:didUpdateLocation:reGeocode:), (IMP)&_logos_method$_ungrouped$LLPunchSettingController$amapLocationManager$didUpdateLocation$reGeocode$, _typeEncoding); }{ class_addMethod(_logos_class$_ungrouped$LLPunchSettingController, @selector(locationManager), (IMP)&_logos_method$_ungrouped$LLPunchSettingController$locationManager$, [[NSString stringWithFormat:@"%s@:", @encode(AMapLocationManager *)] UTF8String]);class_addMethod(_logos_class$_ungrouped$LLPunchSettingController, @selector(setLocationManager:), (IMP)&_logos_method$_ungrouped$LLPunchSettingController$setLocationManager$, [[NSString stringWithFormat:@"v@:%s", @encode(AMapLocationManager *)] UTF8String]);} { class_addMethod(_logos_class$_ungrouped$LLPunchSettingController, @selector(punchConfig), (IMP)&_logos_method$_ungrouped$LLPunchSettingController$punchConfig$, [[NSString stringWithFormat:@"%s@:", @encode(LLPunchConfig *)] UTF8String]);class_addMethod(_logos_class$_ungrouped$LLPunchSettingController, @selector(setPunchConfig:), (IMP)&_logos_method$_ungrouped$LLPunchSettingController$setPunchConfig$, [[NSString stringWithFormat:@"v@:%s", @encode(LLPunchConfig *)] UTF8String]);}  class_addProtocol(_logos_class$_ungrouped$LLPunchSettingController, objc_getProtocol("AMapLocationManagerDelegate")); }} }
#line 184 "/Users/sephilex/Desktop/xiaopao/DingTalk/DingTalkDylib/DingGPS/LLPunchSetting.xm"

#line 1 "/Users/sephilex/Desktop/xiaopao/DingTalk/DingTalkDylib/DingGPS/LLCollectConfig.xm"
#import "DingTalkHelper.h"
#import "LLPunchConfig.h"
#import "LLPunchManager.h"
#import <objc/runtime.h>


#include <substrate.h>
#if defined(__clang__)
#if __has_feature(objc_arc)
#define _LOGOS_SELF_TYPE_NORMAL __unsafe_unretained
#define _LOGOS_SELF_TYPE_INIT __attribute__((ns_consumed))
#define _LOGOS_SELF_CONST const
#define _LOGOS_RETURN_RETAINED __attribute__((ns_returns_retained))
#else
#define _LOGOS_SELF_TYPE_NORMAL
#define _LOGOS_SELF_TYPE_INIT
#define _LOGOS_SELF_CONST
#define _LOGOS_RETURN_RETAINED
#endif
#else
#define _LOGOS_SELF_TYPE_NORMAL
#define _LOGOS_SELF_TYPE_INIT
#define _LOGOS_SELF_CONST
#define _LOGOS_RETURN_RETAINED
#endif

@class DTTableViewController; @class LLCollectConfigController; 
static void (*_logos_orig$_ungrouped$LLCollectConfigController$viewDidLoad)(_LOGOS_SELF_TYPE_NORMAL LLCollectConfigController* _LOGOS_SELF_CONST, SEL); static void _logos_method$_ungrouped$LLCollectConfigController$viewDidLoad(_LOGOS_SELF_TYPE_NORMAL LLCollectConfigController* _LOGOS_SELF_CONST, SEL); static void _logos_method$_ungrouped$LLCollectConfigController$setNavigationBar(_LOGOS_SELF_TYPE_NORMAL LLCollectConfigController* _LOGOS_SELF_CONST, SEL); static void _logos_method$_ungrouped$LLCollectConfigController$tidyDataSource(_LOGOS_SELF_TYPE_NORMAL LLCollectConfigController* _LOGOS_SELF_CONST, SEL); 

#line 6 "/Users/sephilex/Desktop/xiaopao/DingTalk/DingTalkDylib/DingGPS/LLCollectConfig.xm"


static char _logos_property_key$_ungrouped$LLCollectConfigController$refreshSettingBlock;__attribute__((used)) static RefreshSettingBlock _logos_method$_ungrouped$LLCollectConfigController$refreshSettingBlock$(LLCollectConfigController* __unused self, SEL __unused _cmd){ return objc_getAssociatedObject(self, &_logos_property_key$_ungrouped$LLCollectConfigController$refreshSettingBlock); }__attribute__((used)) static void _logos_method$_ungrouped$LLCollectConfigController$setRefreshSettingBlock$(LLCollectConfigController* __unused self, SEL __unused _cmd, RefreshSettingBlock arg){ objc_setAssociatedObject(self, &_logos_property_key$_ungrouped$LLCollectConfigController$refreshSettingBlock, arg, OBJC_ASSOCIATION_COPY_NONATOMIC); }

static void _logos_method$_ungrouped$LLCollectConfigController$viewDidLoad(_LOGOS_SELF_TYPE_NORMAL LLCollectConfigController* _LOGOS_SELF_CONST __unused self, SEL __unused _cmd){
    _logos_orig$_ungrouped$LLCollectConfigController$viewDidLoad(self, _cmd);

    [self setNavigationBar];
    [self tidyDataSource];
}


static void _logos_method$_ungrouped$LLCollectConfigController$setNavigationBar(_LOGOS_SELF_TYPE_NORMAL LLCollectConfigController* _LOGOS_SELF_CONST __unused self, SEL __unused _cmd){
    self.title = @"收藏配置";
    self.view.backgroundColor = [UIColor whiteColor];
}


static void _logos_method$_ungrouped$LLCollectConfigController$tidyDataSource(_LOGOS_SELF_TYPE_NORMAL LLCollectConfigController* _LOGOS_SELF_CONST __unused self, SEL __unused _cmd){

	NSMutableArray <DTCellItem *> *cellItems = [NSMutableArray array];

	NSDirectoryEnumerator *dirEnum = [[NSFileManager defaultManager] enumeratorAtPath:kArchiverFileDoc];
    for (NSString *fileName in dirEnum) {
        NSArray *names = [fileName componentsSeparatedByString:@"config_item_"];
        if (names.count) {
            NSString *alias = [names lastObject];
            DTCellItem *cellItem = [NSClassFromString(@"DTCellItem") cellItemForDefaultStyleWithIcon:nil title:alias detail:nil comment:nil showIndicator:YES cellDidSelectedBlock:^{
                [LLPunchManager shared].defaultConfigFileName = fileName;
                if(self.refreshSettingBlock){
                    self.refreshSettingBlock();
                }
                [self.navigationController popViewControllerAnimated:YES];
            }];
            cellItem.canEdit = YES;
            cellItem.editingStyle = UITableViewCellEditingStyleDelete;
            cellItem.commitEditingBlock = ^(DTBaseCellItem *cellItem,DTCell *cell){
                
                NSString *filePath = [kArchiverFileDoc stringByAppendingPathComponent:fileName];
                NSFileManager *fileManager = [NSFileManager defaultManager];
                if ([fileManager fileExistsAtPath:filePath]) {
                    NSError *error = nil;
                    [fileManager removeItemAtPath:filePath error:&error];
                    if (error) {
                        return;
                    }
                    [self tidyDataSource];
                }
            };
            [cellItems addObject:cellItem];
        }
    }

    DTSectionItem *sectionItem = [NSClassFromString(@"DTSectionItem") itemWithSectionHeader:nil sectionFooter:cellItems.count?@"点选切换配置，左滑可删除配置":@"暂无收藏配置,请先返回上一页添加配置到收藏"];
    sectionItem.dataSource = cellItems;
    
    DTTableViewDataSource *dataSource = [[NSClassFromString(@"DTTableViewDataSource") alloc] init];
    dataSource.tableViewDataSource = @[sectionItem];
    self.dataSource = dataSource;

    [self.tableView reloadData];
}


static __attribute__((constructor)) void _logosLocalInit() {
{Class _logos_class$_ungrouped$DTTableViewController = objc_getClass("DTTableViewController"); { Class _logos_class$_ungrouped$LLCollectConfigController = objc_allocateClassPair(_logos_class$_ungrouped$DTTableViewController, "LLCollectConfigController", 0); objc_registerClassPair(_logos_class$_ungrouped$LLCollectConfigController); MSHookMessageEx(_logos_class$_ungrouped$LLCollectConfigController, @selector(viewDidLoad), (IMP)&_logos_method$_ungrouped$LLCollectConfigController$viewDidLoad, (IMP*)&_logos_orig$_ungrouped$LLCollectConfigController$viewDidLoad);{ char _typeEncoding[1024]; unsigned int i = 0; _typeEncoding[i] = 'v'; i += 1; _typeEncoding[i] = '@'; i += 1; _typeEncoding[i] = ':'; i += 1; _typeEncoding[i] = '\0'; class_addMethod(_logos_class$_ungrouped$LLCollectConfigController, @selector(setNavigationBar), (IMP)&_logos_method$_ungrouped$LLCollectConfigController$setNavigationBar, _typeEncoding); }{ char _typeEncoding[1024]; unsigned int i = 0; _typeEncoding[i] = 'v'; i += 1; _typeEncoding[i] = '@'; i += 1; _typeEncoding[i] = ':'; i += 1; _typeEncoding[i] = '\0'; class_addMethod(_logos_class$_ungrouped$LLCollectConfigController, @selector(tidyDataSource), (IMP)&_logos_method$_ungrouped$LLCollectConfigController$tidyDataSource, _typeEncoding); }{ class_addMethod(_logos_class$_ungrouped$LLCollectConfigController, @selector(refreshSettingBlock), (IMP)&_logos_method$_ungrouped$LLCollectConfigController$refreshSettingBlock$, [[NSString stringWithFormat:@"%s@:", @encode(RefreshSettingBlock)] UTF8String]);class_addMethod(_logos_class$_ungrouped$LLCollectConfigController, @selector(setRefreshSettingBlock:), (IMP)&_logos_method$_ungrouped$LLCollectConfigController$setRefreshSettingBlock$, [[NSString stringWithFormat:@"v@:%s", @encode(RefreshSettingBlock)] UTF8String]);}  }} }
#line 70 "/Users/sephilex/Desktop/xiaopao/DingTalk/DingTalkDylib/DingGPS/LLCollectConfig.xm"
